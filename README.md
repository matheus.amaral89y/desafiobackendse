### Projeto:
* Esse projeto foi realizado usando **SringBoot** como principal framework.
* API Rest
* Banco de dados **SQLite**
* utilizado **factory** para gerenciar as instâncias dos tipos de cálculos e tipos de pagamento. 
  Dessa forma o código fica aberto para **extensão** e fechado para **alterações**.

## Geraração JAR do projeto
* Para gerar o .JAR acesse a pasta desafiobackendse onde está localizado o **gradlew.bat**
* Abre um prompt nesse caminho e execute o comando: **gradlew.bat bootJar**


### Regra de negócio
* O projeto permite escolher entre dois tipos de cálculos para a divisão de contas.
  * Dinheiro/Porcentagem
* Cada pedido pode escolher entre o cálculo ser feito em dinheiro ou em porcentagem.
  * Um pedido não pode ser cálculado com desconto em dinheiro e taxa de serviço em porcentagem por exemplo. Escolha apenas um para cada pedido.

## Documentação da API
### POST: http://localhost:8080/api/v1/order
* Recebe um OrderModel no corpo da requisição.
* {
      "id": 123,
      "type": "APP",
      "discount": 10.5,
      "discountAndServiceChargeType": "CASH",
      "serviceCharge": 5.0,
      "totalPrice": 100.0,
      "products": [
          {
          "id": 1,
          "price": 20.0,
          "productName": "Product A",
          "userId": 101,
          "userName": "User A"
          },
          {
          "id": 2,
          "price": 30.0,
          "productName": "Product B",
          "userId": 102,
          "userName": "User B"
          }
     ]
}

* Cria um pedido e salva esse pedido na tabela Order.
* Faz o vínculo dos produtos selecionados no pedido com a tabela order_products.
* Faz o cálculo de quanto cada colega de trabalho terá que pagar com base no tipo de cálculo escolhido.
* Retorna uma lista de "CalcResponse". O JSON ficará assim:
 * [{
   "orderId": 123,
   "userId": 456,
   "amountToPay": 123.45,
   "description": "Exemplo de descrição"
   }]

### POST: http://localhost:8080/api/v1/charge/pix
* Para usar essa API é necessário um **certificado** fornecido pela Efi e uma **chave publica e privada** para gerar o token.
* Recebe um PixInfoModel no corpo da requisição que contém as informações do devedor e tempo de expiração.
* Utiliza o endpoint: "https://pix-h.api.efipay.com.br/oauth/token" para gerar um token de autorização que será utilizado para as requisções futuras.
  * OBS: pix-h serve para indicar que tudo está sendo feito em homologação, ou seja, nada será cobrado de verdade.
  * A parte de pagamento é uma API externa da Efi Bank.
* Com o token gerado geramos o pagamento PIX utilizando o endpoint: "https://pix-h.api.efipay.com.br/v2/cob".
  * Retorna um PixPaymentModel como resposta.
* PixPaymentModel será usado para realizar um nova requisição para gerar nosso QRCode, usando a informação LOC contida no model.
  * End point para QRCODE: "https://pix-h.api.efipay.com.br/v2/loc/%s/qrcode". Gera um QRCode e retorna um PixPaymentResponse como reposta para o Front End.
  
## Para acessar o frontEnd
* Use o endpoint: "localhost:8080/api/v1"
#### OBS: Apesar do front estar associado ao caminho da API eles não estão acoplados um não depende do outro. Qualquer front que souber os endpoints da API podem utilizar a divisão de pagamento sem problemas.
