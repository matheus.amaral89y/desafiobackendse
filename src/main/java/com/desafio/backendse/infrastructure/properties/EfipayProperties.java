package com.desafio.backendse.infrastructure.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;

@Component
public class EfipayProperties {

    private final ResourceLoader resourceLoader;

    @Value("classpath:certs/homologacao-Certificado.p12")
    private Resource certificate;


    private static final String URL_TOKEN = "https://pix-h.api.efipay.com.br/oauth/token";
    private static final String URL_PIX_COBRANCA_IMEDIATA = "https://pix-h.api.efipay.com.br/v2/cob";
    private static final String URL_PIX_COBRANCA_QRCODE = "https://pix-h.api.efipay.com.br/v2/loc/%s/qrcode";

    public EfipayProperties(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String getAuthUrl() {
        return URL_TOKEN;
    }

    public String getUrlCobPix() {
        return URL_PIX_COBRANCA_IMEDIATA;
    }

    public String getUrlcobPixQrCode() {
        return URL_PIX_COBRANCA_QRCODE;
    }

    public KeyStore getKeyStore() throws Exception {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        try (InputStream inputStream = certificate.getInputStream()) {
            char[] password = null; // Senha nula para certificados sem senha
            keyStore.load(inputStream, password);
        }
        return keyStore;
    }
}