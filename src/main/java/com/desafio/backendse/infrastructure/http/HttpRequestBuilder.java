package com.desafio.backendse.infrastructure.http;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;

public class HttpRequestBuilder {
    private final HttpRequest httpRequest;
    private UriComponentsBuilder uriComponentsBuilder;

    private HttpRequestBuilder() {
        httpRequest = new HttpRequest();
    }

    public static HttpRequestBuilder newBuilder() {
        return new HttpRequestBuilder();
    }

    public HttpRequestBuilder url(String url) {
        httpRequest.setUrl(url);
        return this;
    }

    public HttpRequestBuilder addBody(String body) {
        httpRequest.addBody(body);
        return this;
    }

    public HttpRequestBuilder addQueryParam(String paramName, Object paramValue) {
        if (uriComponentsBuilder == null)
            uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(httpRequest.getUrl());

        if (paramValue == null)
            return this;

        uriComponentsBuilder.queryParam(paramName, paramValue);
        return this;
    }

    public HttpRequestBuilder addContentType(MediaType mediaType) {
        httpRequest.setContentType(mediaType);
        return this;
    }

    public HttpRequestBuilder httpMethod(HttpMethod httpMethod) {
        httpRequest.setHttpMethod(httpMethod);
        return this;
    }

    public HttpRequestBuilder addHeader(String headerName, String headerValue) {
        httpRequest.addHeader(headerName, headerValue);
        return this;
    }

    public HttpRequestBuilder addFormUrlEncodedParameter(String parameterName, String parameterValue) {
        httpRequest.addFormUrlEncodedParameter(parameterName, parameterValue);
        return this;
    }

    public HttpRequest build() {
        if (uriComponentsBuilder != null)
            httpRequest.setUrl(uriComponentsBuilder.build().toString());

        return httpRequest;
    }
}
