package com.desafio.backendse.infrastructure.http;

import com.desafio.backendse.exceptions.HttpStatusException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.impl.DefaultRedirectStrategy;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Setter
@Getter
@Slf4j
public class HttpRequest {

    private String url;
    private HttpMethod httpMethod;
    private MultiValueMap<String, String> urlEncodedparameters;
    private HttpHeaders httpHeaders;
    private MediaType mediaType;
    private String body;

    public HttpRequest() {
        this.urlEncodedparameters = new LinkedMultiValueMap<>();
        this.httpHeaders = new HttpHeaders();
    }

    public void setContentType(MediaType mediaType) {
        this.httpHeaders.setContentType(mediaType);
    }

    public void addHeader(String headerName, String headerValue) {
        httpHeaders.add(headerName, headerValue);
    }

    public void addFormUrlEncodedParameter(String parameterName, String parameterValue) {
        urlEncodedparameters.add(parameterName, parameterValue);
    }

    public void addBody(String body) {
        this.body = body;
    }

    public String request() throws HttpStatusException {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<?> request = new HttpEntity<>(urlEncodedparameters, httpHeaders);
        if (body != null)
            request = new HttpEntity<>(body, httpHeaders);

        try {

            return request(restTemplate, request);

        } catch (HttpClientErrorException e) {
            log.error("Erro ao realizar request", e);
            throw new HttpStatusException(e.getStatusCode(), e.getStatusText());
        }
    }

    private String request(RestTemplate restTemplate, HttpEntity<?> request) {
        ResponseEntity<String> response = restTemplate.exchange(url, httpMethod, request, String.class);

        if (response.getStatusCode().is2xxSuccessful())
            return response.getBody();

        throw new HttpStatusException(response.getStatusCode(), response.getBody());
    }
}
