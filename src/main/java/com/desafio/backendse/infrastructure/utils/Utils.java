package com.desafio.backendse.infrastructure.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class Utils {
    private Utils(){

    }
    public static <T> T convertFromJson(String json, Class<T> target){
        try {
            ObjectMapper map = new ObjectMapper();
            return map.readValue(json, target);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao converter JSON para objeto", e);
        }
    }
    public static <T> String convertToJson(T object) {
        try {
            ObjectMapper map = new ObjectMapper();
            return map.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao converter objeto para JSON", e);
        }
    }

}
