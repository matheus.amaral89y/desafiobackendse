package com.desafio.backendse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendseApplication.class, args);
	}

}
