package com.desafio.backendse.application.mapper;

import com.desafio.backendse.domain.entities.OrderProducts;
import com.desafio.backendse.application.model.OrderModel;

import java.util.List;
import java.util.stream.Collectors;

public class OrderProductsMap {
    public static List<OrderProducts> mapToEntity(OrderModel model) {
        return model.getProducts().stream()
                .map(productModel -> {
                    OrderProducts orderProducts = new OrderProducts();
                    orderProducts.setOrderId(model.getId());
                    orderProducts.setProductId(productModel.getId());
                    orderProducts.setUserId(productModel.getUserId());
                    return orderProducts;
                })
                .collect(Collectors.toList());
    }
}
