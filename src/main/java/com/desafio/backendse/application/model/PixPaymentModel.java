package com.desafio.backendse.application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PixPaymentModel {
    @JsonProperty("calendario")
    private Calendar calendar;

    @JsonProperty("txid")
    private String txid;

    @JsonProperty("revisao")
    private int revision;

    @JsonProperty("status")
    private String status;

    @JsonProperty("valor")
    private Price price;

    @JsonProperty("chave")
    private String key;

    @JsonProperty("devedor")
    private Debtor debtor;

    @JsonProperty("solicitacaoPagador")
    private String payerRequest;

    @JsonProperty("loc")
    private Loc loc;

    @JsonProperty("location")
    private String location;

    @JsonProperty("pixCopiaECola")
    private String pixCopyAndPaste;

    @Getter
    @Setter
    public static class Calendar {
        @JsonProperty("criacao")
        private Date creation;

        @JsonProperty("expiracao")
        private int expiration;
    }

    @Getter
    @Setter
    public static class Price {
        @JsonProperty("original")
        private String original;
    }

    @Getter
    @Setter
    public static class Debtor {
        @JsonProperty("cpf")
        private String cpf;

        @JsonProperty("nome")
        private String name;
    }

    @Getter
    @Setter
    public static class Loc {
        @JsonProperty("id")
        private int id;

        @JsonProperty("location")
        private String location;

        @JsonProperty("tipoCob")
        private String Cobtype;

        @JsonProperty("criacao")
        private Date creation;
    }
}
