package com.desafio.backendse.application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class ProductModel {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("price")
    private BigDecimal price;

    @JsonProperty("productName")
    private String productName;

    @JsonProperty("userId")
    private Long userId;

    @JsonProperty("userName")
    private String userName;
}
