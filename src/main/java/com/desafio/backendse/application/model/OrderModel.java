package com.desafio.backendse.application.model;

import com.desafio.backendse.domain.entities.Order;
import com.desafio.backendse.domain.enums.DiscountAndServiceChargeType;
import com.desafio.backendse.domain.enums.OrderType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderModel {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("type")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OrderType type;

    @JsonProperty("discount")
    private BigDecimal discount;

    @JsonProperty("discountAndServiceChargeType")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private DiscountAndServiceChargeType discountAndServiceChargeType;

    @JsonProperty("serviceCharge")
    private BigDecimal serviceCharge;

    @JsonProperty("totalPrice")
    private BigDecimal totalPrice;

    @JsonProperty("products")
    private List<ProductModel> products;

    public static Order mapToEntity(OrderModel model){
        Order order = new Order();
        order.setDiscount(model.getDiscount());
        order.setType(model.getType());
        order.setDiscountAndServiceChargeType(model.getDiscountAndServiceChargeType());
        order.setServiceCharge(model.getServiceCharge());
        order.setTotalPrice(model.getTotalPrice());

        return order;
    }
}
