package com.desafio.backendse.application.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PixInfoModel {
    @JsonProperty("calendario")
    private Calendar calendar;

    @JsonProperty("devedor")
    private Debtor debtor;

    @JsonProperty("valor")
    private Price price;

    @JsonProperty("chave")
    private String key;

    @JsonProperty("solicitacaoPagador")
    private String payerRequest;

    public static class Calendar {
        @JsonProperty("expiracao")
        private int expiration;
    }

    public static class Debtor {
        @JsonProperty("cpf")
        private String cpf;
        @JsonProperty("nome")
        private String name;
    }

    public static class Price {
        @JsonProperty("original")
        private String original;
    }
}