package com.desafio.backendse.application.controller;

import com.desafio.backendse.application.model.PixInfoModel;
import com.desafio.backendse.application.model.TokenModel;
import com.desafio.backendse.application.model.PixPaymentModel;
import com.desafio.backendse.application.reponses.PixPaymentResponse;
import com.desafio.backendse.domain.services.PaymentServices;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("charge")
@Slf4j
public class PixGeneratorController {
    private final PaymentServices paymentServices;

    public PixGeneratorController(PaymentServices paymentServices) {
        this.paymentServices = paymentServices;
    }

    @PostMapping("/pix")
    public ResponseEntity<PixPaymentResponse> createPaymentPix(@RequestBody PixInfoModel model) {
        try {
            log.info("Gerando cobrança PIX Copia e cola");

            TokenModel tokenModel = paymentServices.getToken();
            String authToken = String.format("%s %s", tokenModel.getTokenType() ,tokenModel.getToken());

            PixPaymentModel paymentModel = paymentServices.generatePaymentPix(model, authToken);

            log.info("Cobrança PIX copia e cola criada: ", paymentModel);
            log.info("Gerando cobrança PIX por QRCode");

            PixPaymentResponse pixPaymentResponse = paymentServices.generatePixQrCode(authToken, paymentModel.getLoc().getId());

            return new ResponseEntity<>(pixPaymentResponse, HttpStatus.OK);
        } catch(Exception ex) {
            log.error("Erro ao criar pagamento PIX", ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
