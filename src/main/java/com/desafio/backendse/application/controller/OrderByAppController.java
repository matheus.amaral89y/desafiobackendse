package com.desafio.backendse.application.controller;

import com.desafio.backendse.application.reponses.CalcResponse;
import com.desafio.backendse.domain.entities.Order;
import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.domain.services.OrderServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("order")
@Slf4j
public class OrderByAppController {
    private final OrderServices orderServices;

    public OrderByAppController(OrderServices orderServices) {
        this.orderServices = orderServices;
    }

    @PostMapping
    public ResponseEntity<List<CalcResponse>> createOrder(@RequestBody OrderModel model){
        try {
            log.info("Chamada recebida pra criar o pedido: {}", model);
            Order order = OrderModel.mapToEntity(model);

            model.setId(orderServices.save(order));
            orderServices.linkOrderWithProduct(model);
            List<CalcResponse> response = orderServices.calculatePaymentAmount(model);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            log.error("Erro ao criar pedido: {}", model, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
