package com.desafio.backendse.application.reponses;

import com.desafio.backendse.domain.entities.Products;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
public class CalcResponse {
    private Long orderId;
    private Long userId;
    private BigDecimal amountToPay;
    private String description;
}
