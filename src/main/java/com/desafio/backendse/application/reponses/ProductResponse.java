package com.desafio.backendse.application.reponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductResponse {
    @JsonProperty("id")
    private long id;
    @JsonProperty("price")
    private BigDecimal price;
    @JsonProperty("product_name")
    private String productName;
}
