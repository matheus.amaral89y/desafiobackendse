package com.desafio.backendse.application.reponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PixPaymentResponse {
    @JsonProperty("qrcode")
    private String qrcode;

    @JsonProperty("imagemQrcode")
    private String qrCodeImage;

    @JsonProperty("linkVisualizacao")
    private String visualizationLink;
}
