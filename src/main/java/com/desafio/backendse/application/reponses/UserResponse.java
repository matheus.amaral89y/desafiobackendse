package com.desafio.backendse.application.reponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponse {
    @JsonProperty("id")
    private long id;
    @JsonProperty("name")
    private String name;
}
