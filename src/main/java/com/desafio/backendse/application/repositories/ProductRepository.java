package com.desafio.backendse.application.repositories;

import com.desafio.backendse.domain.entities.Products;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends BaseRepository<Products>{
}
