package com.desafio.backendse.application.repositories;

import com.desafio.backendse.domain.entities.EntityBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository <T extends EntityBase> extends JpaRepository<T, Integer> {
}
