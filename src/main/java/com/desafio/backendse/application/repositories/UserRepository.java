package com.desafio.backendse.application.repositories;

import com.desafio.backendse.domain.entities.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<Users>{
}
