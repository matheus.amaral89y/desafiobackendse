package com.desafio.backendse.application.repositories;

import com.desafio.backendse.domain.entities.OrderProducts;

public interface OrderProductsRepository extends BaseRepository<OrderProducts>{
}
