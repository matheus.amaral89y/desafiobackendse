package com.desafio.backendse.application.repositories;

import com.desafio.backendse.domain.entities.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends BaseRepository<Order>{
}
