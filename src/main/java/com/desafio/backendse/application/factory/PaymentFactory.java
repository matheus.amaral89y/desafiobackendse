package com.desafio.backendse.application.factory;

import com.desafio.backendse.domain.enums.PaymentType;
import com.desafio.backendse.domain.payments.GeneratePayment;
import com.desafio.backendse.domain.payments.PixPayment;
import com.desafio.backendse.infrastructure.properties.EfipayProperties;
import org.springframework.stereotype.Component;

@Component
public class PaymentFactory {
    private final EfipayProperties efipayProperties;

    public PaymentFactory(EfipayProperties efipayProperties) {
        this.efipayProperties = efipayProperties;
    }

    public GeneratePayment getPaymentType(PaymentType type){
        return switch (type) {
            case PIX -> new PixPayment(efipayProperties);
        };
    }
}
