package com.desafio.backendse.application.factory;

import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.domain.calculations.BaseCalc;
import com.desafio.backendse.domain.calculations.CashCalc;
import com.desafio.backendse.domain.calculations.PercentageCalc;
import com.desafio.backendse.domain.enums.DiscountAndServiceChargeType;

public class CalcTypeFactory {
    private final DiscountAndServiceChargeType type;
    private final OrderModel model;
    public CalcTypeFactory(DiscountAndServiceChargeType type, OrderModel model){
        this.type = type;
        this.model = model;
    }

    public BaseCalc getCalculateType(){
        return switch (type) {
            case CASH -> new CashCalc(model);
            case PERCENT -> new PercentageCalc(model);
        };
    }
}
