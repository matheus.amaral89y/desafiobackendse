package com.desafio.backendse.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatusCode;

@Getter
public class HttpStatusException extends RuntimeException {
    private final HttpStatusCode statusCode;
    private final String responseBody;

    public HttpStatusException(HttpStatusCode statusCode, String responseBody) {
        super("HTTP request failed with status code: " + statusCode.value() + "body: " + responseBody);
        this.statusCode = statusCode;
        this.responseBody = responseBody;
    }

}
