package com.desafio.backendse.domain.payments;

import com.desafio.backendse.infrastructure.properties.EfipayProperties;

import java.io.IOException;

public abstract class GeneratePayment extends AuthBase{
    public abstract String createPayment(String json, String token) throws IOException;
    protected GeneratePayment(EfipayProperties efipayProperties) { super(efipayProperties); }
    protected String getUrlCobPix(){
        return efipayProperties.getUrlCobPix();
    }
    protected String getUrlcobPixQrCode() { return efipayProperties.getUrlcobPixQrCode(); };
}
