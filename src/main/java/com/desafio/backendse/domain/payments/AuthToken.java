package com.desafio.backendse.domain.payments;

import com.desafio.backendse.infrastructure.http.HttpRequestBuilder;
import com.desafio.backendse.infrastructure.properties.EfipayProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Base64;

@Slf4j
@Component
public class AuthToken extends AuthBase{
    private final String CLIENT_ID = "Client_Id_d164dc67516ae95e25b2c95ff1fd3d58d025a6c3";
    private final String CLIENT_SECRET = "Client_Secret_a9469d3e2454a5546faa1e3af0efaf3ed1063d12";

    public AuthToken(EfipayProperties efipayProperties) {
        super(efipayProperties);
    }

    public String getToken() throws IOException {
        addCertificate();
        String basicAuth = Base64.getEncoder().encodeToString(((CLIENT_ID+':'+CLIENT_SECRET).getBytes()));

        var requestBuilder = HttpRequestBuilder.newBuilder()
                .url(authUrl())
                .addHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))
                .addHeader("Authorization", String.format("Basic %s", basicAuth))
                .addBody("{\"grant_type\": \"client_credentials\"}")
                .httpMethod(HttpMethod.POST).build();

        log.info("Gerando token de autenticação");
        log.info(requestBuilder.request());

        return requestBuilder.request();
    }
}
