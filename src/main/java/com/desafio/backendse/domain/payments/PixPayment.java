package com.desafio.backendse.domain.payments;

import com.desafio.backendse.infrastructure.http.HttpRequestBuilder;
import com.desafio.backendse.infrastructure.properties.EfipayProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.io.IOException;

@Slf4j
public class PixPayment extends GeneratePayment{
    public PixPayment(EfipayProperties efipayProperties) {
        super(efipayProperties);
    }

    @Override
    public String createPayment(String json, String token) throws IOException {
        addCertificate();

        var requestBuilder = HttpRequestBuilder.newBuilder()
                .url(getUrlCobPix())
                .addBody(json)
                .addHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))
                .addHeader("authorization", token)
                .httpMethod(HttpMethod.POST)
                .build();

        log.info("Executando geração do pagamento pix");
        log.info(requestBuilder.request());

        return requestBuilder.request();
    }

    public String generatePixQrCode(String token, Integer locId) throws IOException {
        addCertificate();

        var requestBuilder = HttpRequestBuilder.newBuilder()
                .url(String.format(getUrlcobPixQrCode(), locId))
                .addHeader("authorization", token)
                .httpMethod(HttpMethod.GET)
                .build();

        return requestBuilder.request();
    }
}
