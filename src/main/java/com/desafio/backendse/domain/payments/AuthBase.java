package com.desafio.backendse.domain.payments;

import com.desafio.backendse.infrastructure.properties.EfipayProperties;
import org.springframework.stereotype.Component;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.IOException;

@Component
public abstract class AuthBase {
    protected final EfipayProperties efipayProperties;

    protected AuthBase(EfipayProperties efipayProperties) {
        this.efipayProperties = efipayProperties;
    }

    protected void addCertificate() throws IOException {
        try {
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(efipayProperties.getKeyStore(), null);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), null, null);

            SSLContext.setDefault(sslContext);
        } catch (Exception e) {
            throw new IOException("Erro ao adicionar o certificado", e);
        }
    }

    protected String authUrl(){
        return efipayProperties.getAuthUrl();
    }
}
