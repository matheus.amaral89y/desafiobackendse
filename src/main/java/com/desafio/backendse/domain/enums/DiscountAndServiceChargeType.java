package com.desafio.backendse.domain.enums;

import lombok.Getter;

@Getter
public enum DiscountAndServiceChargeType {
    PERCENT,
    CASH;
}
