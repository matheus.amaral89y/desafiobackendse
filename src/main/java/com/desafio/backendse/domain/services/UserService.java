package com.desafio.backendse.domain.services;

import com.desafio.backendse.application.reponses.UserResponse;
import com.desafio.backendse.application.repositories.UserRepository;
import com.desafio.backendse.domain.entities.Users;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserResponse> getUsers(){
        List<Users> usersList = userRepository.findAll();
        List<UserResponse> userResponseList = usersList.stream()
                .map(this::convertToUserResponse)
                .collect(Collectors.toList());

        return userResponseList;
    }

    private UserResponse convertToUserResponse(Users users){
        UserResponse userResponse = new UserResponse();
        userResponse.setId(users.getId());
        userResponse.setName(users.getName());

        return userResponse;
    }
}
