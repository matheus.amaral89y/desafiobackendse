package com.desafio.backendse.domain.services;

import com.desafio.backendse.application.reponses.ProductResponse;
import com.desafio.backendse.application.repositories.ProductRepository;
import com.desafio.backendse.domain.entities.Products;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<ProductResponse> getProducts(){
        List<Products> products = productRepository.findAll();

        List<ProductResponse> productResponses = products.stream()
                .map(this::convertToProductResponse)
                .collect(Collectors.toList());

        return productResponses;
    }

    private ProductResponse convertToProductResponse(Products product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(product.getId());
        productResponse.setPrice(product.getPrice());
        productResponse.setProductName(product.getProductName());
        return productResponse;
    }
}
