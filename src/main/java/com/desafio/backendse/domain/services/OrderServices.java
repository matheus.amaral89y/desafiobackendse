package com.desafio.backendse.domain.services;

import com.desafio.backendse.application.factory.CalcTypeFactory;
import com.desafio.backendse.application.reponses.CalcResponse;
import com.desafio.backendse.application.repositories.OrderProductsRepository;
import com.desafio.backendse.application.repositories.OrderRepository;
import com.desafio.backendse.domain.entities.Order;
import com.desafio.backendse.domain.entities.OrderProducts;
import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.application.mapper.OrderProductsMap;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class OrderServices {
    private final OrderRepository orderRepository;
    private final OrderProductsRepository orderProductsRepository;

    public OrderServices(OrderRepository orderRepository, OrderProductsRepository orderProductsRepository) {
        this.orderRepository = orderRepository;
        this.orderProductsRepository = orderProductsRepository;
    }

    @Transactional
    public Long save(Order order){
        this.orderRepository.save(order);

        log.info("Pedido salvo no ID: {}", order.getId());

        return order.getId();
    }

    public void linkOrderWithProduct(OrderModel model){
        log.info("Estabelecendo vínculo do pedido com os produtos.");
        for (OrderProducts orderProducts : OrderProductsMap.mapToEntity(model)){
            orderProductsRepository.save(orderProducts);
        }
    }

    public List<CalcResponse> calculatePaymentAmount(OrderModel model){
        log.info("Calculando o valor a pagar do pedido.");

        CalcTypeFactory factory = new CalcTypeFactory(model.getDiscountAndServiceChargeType(), model);
        return factory.getCalculateType().calc();
    }
}
