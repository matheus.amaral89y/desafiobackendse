package com.desafio.backendse.domain.services;

import com.desafio.backendse.application.factory.PaymentFactory;
import com.desafio.backendse.application.model.PixInfoModel;
import com.desafio.backendse.application.model.PixPaymentModel;
import com.desafio.backendse.application.model.TokenModel;
import com.desafio.backendse.application.reponses.PixPaymentResponse;
import com.desafio.backendse.domain.enums.PaymentType;
import com.desafio.backendse.domain.payments.AuthToken;
import com.desafio.backendse.domain.payments.PixPayment;
import com.desafio.backendse.infrastructure.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;

@Service
public class PaymentServices {
    private final PaymentFactory paymentFactory;
    private final AuthToken authToken;

    public PaymentServices(PaymentFactory paymentFactory, AuthToken authToken, ObjectMapper objectMapper) {
        this.paymentFactory = paymentFactory;
        this.authToken = authToken;
    }

    public PixPaymentModel generatePaymentPix(PixInfoModel model, String token) throws IOException {
        String json = paymentFactory.getPaymentType(PaymentType.PIX).createPayment(Utils.convertToJson(model), token);
        return Utils.convertFromJson(json, PixPaymentModel.class);
    }

    public PixPaymentResponse generatePixQrCode(String token, Integer locId) throws IOException {
        PixPayment paymentType = (PixPayment)paymentFactory.getPaymentType(PaymentType.PIX);
        String json = paymentType.generatePixQrCode(token, locId);

        return Utils.convertFromJson(json, PixPaymentResponse.class);
    }

    public TokenModel getToken() throws IOException {
        return  Utils.convertFromJson(authToken.getToken(), TokenModel.class);
    }
}
