package com.desafio.backendse.domain.calculations;

import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.application.model.ProductModel;
import com.desafio.backendse.application.reponses.CalcResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public abstract class BaseCalc {
    protected final OrderModel orderModel;
    protected final BigDecimal HUNDRED_PER_CENT = BigDecimal.valueOf(100);
    protected final int NUMBER_DECIMAL_PLACES = 2;
    protected BaseCalc(OrderModel model){
        this.orderModel = model;
    }

    public abstract List<CalcResponse> calc();
    protected Map<Long, BigDecimal> calculatePaymentPerPerson() {
        return calculateUserTotalPrice(orderModel.getProducts(), orderModel.getTotalPrice());
    }

    private Map<Long, BigDecimal> calculateUserTotalPrice(List<ProductModel> productModelList, BigDecimal totalPrice) {
        Map<Long, BigDecimal> userTotalPriceMap = new HashMap<>();

        productModelList.forEach(productModel -> {
            long userId = productModel.getUserId();
            BigDecimal oldPrice = productModel.getPrice();

            userTotalPriceMap.merge(userId, oldPrice, BigDecimal::add);
        });

        userTotalPriceMap.forEach((userId, totalUserPrice) -> {
            BigDecimal percentage = totalUserPrice.multiply(HUNDRED_PER_CENT).divide(totalPrice, NUMBER_DECIMAL_PLACES, RoundingMode.HALF_UP);
            userTotalPriceMap.put(userId, percentage);
        });

        return userTotalPriceMap;
    }

    protected CalcResponse buildResponse(BigDecimal amountToPay, long userId){
        List<ProductModel> products = orderModel.getProducts();
        var userName = products.stream().filter(p -> Objects.equals(p.getUserId(), userId)).findFirst().map(ProductModel::getUserName).orElse("");

        CalcResponse response = new CalcResponse();
        response.setAmountToPay(amountToPay);
        response.setUserId(userId);
        response.setOrderId(orderModel.getId());
        response.setDescription(String.format("O valor a pagar do colaborador %s é de: %s.", userName, response.getAmountToPay()));

        return response;
    }

    protected List<CalcResponse> finalAmountPayable(Map<Long, BigDecimal> userTotalPriceMap, BigDecimal updatedValue){
        List<CalcResponse> calcResponseList = new ArrayList<>();

        userTotalPriceMap.forEach((userId, totalUserPrice) -> {
            BigDecimal amountToPay = totalUserPrice.multiply(updatedValue).divide(HUNDRED_PER_CENT, NUMBER_DECIMAL_PLACES, RoundingMode.HALF_UP);
            calcResponseList.add(buildResponse(amountToPay, userId));
        });

        return calcResponseList;
    }
}
