package com.desafio.backendse.domain.calculations;

import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.application.model.ProductModel;
import com.desafio.backendse.application.reponses.CalcResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CashCalc extends BaseCalc {
    public CashCalc(OrderModel model) {
        super(model);
    }

    @Override
    public List<CalcResponse> calc() {
        BigDecimal updatedValue = orderModel.getTotalPrice()
                .subtract(orderModel.getDiscount())
                .add(orderModel.getServiceCharge());

        return finalAmountPayable(calculatePaymentPerPerson(), updatedValue);
    }
}
