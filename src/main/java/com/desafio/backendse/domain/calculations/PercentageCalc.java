package com.desafio.backendse.domain.calculations;

import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.application.reponses.CalcResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PercentageCalc extends BaseCalc {
    public PercentageCalc(OrderModel model) {
        super(model);
    }

    @Override
    public List<CalcResponse> calc() {
        BigDecimal updatedValue = orderModel.getTotalPrice()
                .multiply(HUNDRED_PER_CENT.subtract(orderModel.getDiscount()))
                .divide(HUNDRED_PER_CENT, NUMBER_DECIMAL_PLACES, RoundingMode.HALF_UP)
                .multiply(HUNDRED_PER_CENT.add(orderModel.getServiceCharge()))
                .divide(HUNDRED_PER_CENT, NUMBER_DECIMAL_PLACES, RoundingMode.HALF_UP);

        return finalAmountPayable(calculatePaymentPerPerson(), updatedValue);
    }
}
