package com.desafio.backendse.domain.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class OrderProducts extends EntityBase {
    @Column
    private Long orderId;
    @Column
    private Long productId;
    @Column
    private Long userId;
}
