package com.desafio.backendse.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Products extends EntityBase{
    @Column
    private String productName;
    @Column
    private BigDecimal price;
}
