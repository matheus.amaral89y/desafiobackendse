package com.desafio.backendse.domain.entities;

import com.desafio.backendse.domain.enums.DiscountAndServiceChargeType;
import com.desafio.backendse.domain.enums.OrderType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order extends EntityBase{
    @Column
    @Enumerated(EnumType.STRING)
    private OrderType type;
    @Column
    private BigDecimal totalPrice;
    @Column
    private BigDecimal discount;
    @Column
    private BigDecimal serviceCharge;
    @Column
    @Enumerated(EnumType.STRING)
    private DiscountAndServiceChargeType discountAndServiceChargeType;
}
