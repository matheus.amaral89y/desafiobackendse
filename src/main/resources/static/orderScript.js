const productObjects = [];
const userObjects = [];
const productVinculo = [];
let valorDesconto = 0;
let taxaDeServico = 0;
let valorTotal = 0;

const orderData = {
    id: 0,
    type: "APP",
    discount: 0,
    discountAndServiceChargeType: "CASH",
    serviceCharge: 0,
    totalPrice: 0,
    products: null
};

function habilitarProduto() {
    const amigosDropdown = document.getElementById("amigos");
    const produtosDropdown = document.getElementById("produtos");

    // Habilita o dropdown de produtos apenas se um amigo for selecionado
    produtosDropdown.disabled = amigosDropdown.value === "selecione";
}

function adicionarPedido() {
    const tipoPedido = document.getElementById("tipoPedido").value;
    const tipoDesconto = document.getElementById("tipoDesconto").value;

    const amigoSelecionadoId = document.getElementById("amigos").value;
    const amigoSelecionado = userObjects.find(user => user.id == amigoSelecionadoId);

    const produtoSelecionadoId  = document.getElementById("produtos").value;
    const produtoSelecionado = productObjects.find(product => product.id == produtoSelecionadoId);

    if (amigoSelecionado.name === "selecione" || produtoSelecionado.productName  === "selecione") {
        alert("Selecione um amigo e um produto antes de adicionar o pedido.");
        return;
    }

    if (tipoDesconto === "selecione") {
        alert("Selecione um tipo de desconto.");
        return;
    }

    if (tipoPedido === "selecione") {
        alert("Selecione um tipo de pedido.");
        return;
    }

    // Verifica se o amigo já está na tabela para evitar duplicidades
    const tabelaPedidos = document.getElementById("tabelaPedidos");
    const linhasTabela = tabelaPedidos.getElementsByTagName("tr");

    for (const linha of linhasTabela) {
        const colunas = linha.getElementsByTagName("td");
        if (colunas.length >= 2 && colunas[0].textContent === amigoSelecionado.name && colunas[1].textContent === produtoSelecionado.productName) {
            alert("Você já adicionou um pedido para este amigo com o mesmo produto.");
            return;
        }
    }

    valorTotal = valorTotal + produtoSelecionado.price;
    const objeto = {id: produtoSelecionadoId, price: produtoSelecionado.price, productName: produtoSelecionado.productName, userId: amigoSelecionadoId, userName: amigoSelecionado.name}
    productVinculo.push(objeto);

    // Adiciona a linha na tabela
    const novaLinha = tabelaPedidos.insertRow();
    const colunaAmigo = novaLinha.insertCell(0);
    const colunaProduto = novaLinha.insertCell(1);

    colunaAmigo.textContent = amigoSelecionado.name;
    colunaProduto.textContent = produtoSelecionado.productName;

    // Limpa as seleções
    document.getElementById("amigos").value = "selecione";
    document.getElementById("produtos").value = "selecione";
    document.getElementById("produtos").disabled = true;
}

async function enviarPedido(){
    const tipoPedido = document.getElementById("tipoPedido").value;
    const tipoDesconto = document.getElementById("tipoDesconto").value;

    const amigoSelecionadoId = document.getElementById("amigos").value;
    const amigoSelecionado = userObjects.find(user => user.id == amigoSelecionadoId);

    const produtoSelecionadoId  = document.getElementById("produtos").value;
    const produtoSelecionado = productObjects.find(product => product.id == produtoSelecionadoId);

    if (tipoDesconto === "selecione") {
        alert("Selecione um tipo de desconto.");
        return;
    }

    if (tipoPedido === "selecione") {
        alert("Selecione um tipo de pedido.");
        return;
    }

    if (productVinculo.length === 0) {
        alert("Escolha um produto antes de enviar o pedido.");
        return;
    }

    class OrderModel {
        constructor(id, type, discount, discountAndServiceChargeType, serviceCharge, totalPrice, products) {
            this.id = id;
            this.type = type;
            this.discount = discount;
            this.discountAndServiceChargeType = discountAndServiceChargeType;
            this.serviceCharge = serviceCharge;
            this.totalPrice = totalPrice;
            this.products = products.map(product => new ProductModel(product.id, product.price, product.productName, product.userId, product.userName));
        }
    }

    class ProductModel {
        constructor(id, price, productName, userId, userName) {
            this.id = id;
            this.price = price;
            this.productName = productName;
            this.userId = userId;
            this.userName = userName;
        }
    }

    const orderModelInstance = new OrderModel(0, document.getElementById("tipoPedido").value, valorDesconto, document.getElementById("tipoDesconto").value,
                                              taxaDeServico, valorTotal, productVinculo);

    console.log(orderModelInstance);

    var tabela = document.getElementById("tabelaPedidos");
    var linhas = tabela.getElementsByTagName("tr");

    for (var i = linhas.length - 1; i > 0; i--) {
        var pai = linhas[i].parentNode;
        pai.removeChild(linhas[i]);
    }

    productVinculo.length = 0;
    valorDesconto = 0;
    taxaDeServico = 0;
    valorTotal = 0;

    const url = 'http://localhost:8080/api/v1/order';
    const urlPix = 'http://localhost:8080/api/v1/charge/pix';

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(orderModelInstance),
        });

        if (response.ok) {
            const responseData = await response.json();
            console.log('Resposta do servidor:', responseData);

            var responsePix;

            try  {
                let valorPagamento = 0;
                let nomePagador = ""
                responseData.forEach(function (result) {
                    valorPagamento = valorPagamento + result.amountToPay;
                });

                const pixData = {
                    calendario: {
                        expiracao: 3600
                    },
                    devedor: {
                        cpf: "12345678909",
                        nome: "pagadorTeste"
                    },
                    valor: {
                        original: `${formatarValor(valorPagamento)}`
                    },
                    chave: "chavePixTeste@gmail.com.br"
                };

                responsePix = await fetch(urlPix, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(pixData),
                });

                if (responsePix.ok) {
                    const responsePixData = await responsePix.json();
                    console.log('Resposta do servidor:', responsePixData);

                    exibirResultadoPedido(responseData, responsePixData);
                }
            } catch (error){
                console.error("Erro ao gerar pagamento pix: ", responsePix.status, responsePix);
            }
        } else {
            console.error('Erro no pedido:', response.status, response.statusText);
        }
    } catch (error) {
        console.error('Erro ao enviar o pedido:', error);
    }
}

async function fetchProducts() {
    try {
        // Faz a requisição à API
        const response = await fetch('http://localhost:8080/api/v1/product');

        // Verifica se a requisição foi bem-sucedida
        if (!response.ok) {
            throw new Error('Erro ao buscar produtos');
        }

        // Converte a resposta para JSON
        const productsArray = await response.json();

        // Seleciona o elemento <select> pelo ID
        const produtosSelect = document.getElementById("produtos");

        for (const product of productsArray) {
            const productObject = {
                id: product.id,
                price: product.price,
                productName: product.product_name
            };

            productObjects.push(productObject);
        }

        for (const product of productObjects) {
            // Adiciona uma nova opção ao <select> com os detalhes do produto
            const optionProduto = document.createElement("option");
            optionProduto.value = product.id;
            optionProduto.text = `${product.productName} - R$${product.price}`;
            produtosSelect.appendChild(optionProduto);
        }
    }
     catch (error) {
        console.error(error.message);
    }
}

async function fetchUsers() {
    try {
        // Faz a requisição à API
        const response = await fetch('http://localhost:8080/api/v1/user');

        // Verifica se a requisição foi bem-sucedida
        if (!response.ok) {
            throw new Error('Erro ao buscar usuários');
        }

        // Converte a resposta para JSON
        const usersArray = await response.json();

        // Seleciona o elemento <select> pelo ID
        const usersSelect = document.getElementById("amigos");

        for (const user of usersArray) {
            const userObject = {
                id: user.id,
                name: user.name
            };

            userObjects.push(userObject);
        }

        for (const user of userObjects) {
            // Adiciona uma nova opção ao <select> com os detalhes do produto
            const optionUser = document.createElement("option");
            optionUser.value = user.id;
            optionUser.text = `${user.name}`;
            usersSelect.appendChild(optionUser);
        }
    }
     catch (error) {
        console.error(error.message);
    }
}

function descontoChange(valor){
    let valorFormatadoDesconto = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'}).format(valor);
    valorDesconto = valorFormatadoDesconto.replace('$', '').replace(',', '');
}

function taxaDeServicoChange(valor){
    let valorFormatadoTaxa = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'}).format(valor);
    taxaDeServico = valorFormatadoTaxa.replace('$', '').replace(',', '');
}

function exibirResultadoPedido(responseData, responsePixData) {
    const imagemQrdCode = responsePixData.imagemQrcode;

    var containerResultado = document.getElementById('containerResultado');

    containerResultado.innerHTML = '';

    responseData.forEach(function (result) {
        var divResultado = document.createElement('div');
        divResultado.className = 'resultadoPedido';

        divResultado.innerHTML = `
            <p>Amigo: ${result.userId}</p>
            <p>Valor a Pagar: ${result.amountToPay}</p>
            <p>Descrição: ${result.description}</p>
        `;
        containerResultado.appendChild(divResultado);
    });

    // Adiciona a descrição sobre o QR Code de homologação
    var divDescricaoHomologacao = document.createElement('div');
    divDescricaoHomologacao.innerHTML = '<p>Aponte a câmera do celular para realizar o pagamento</p>';
    containerResultado.appendChild(divDescricaoHomologacao);

    // Adiciona a imagem QR Code
    var divImagemQrdCode = document.createElement('div');
    divImagemQrdCode.innerHTML = `<img src="${imagemQrdCode}" alt="QR Code">`;
    containerResultado.appendChild(divImagemQrdCode);
}

function formatarValor(valorStr) {
    try {
        const valorFloat = parseFloat(valorStr);
        // Verifica se o valor já possui casas decimais
        if (Number.isInteger(valorFloat)) {
            return `${parseInt(valorFloat)}.00`;
        } else {
            return valorStr;  // Não faz nada se já tiver casas decimais
        }
    } catch (error) {
        return "Erro: Valor inválido";
    }
}