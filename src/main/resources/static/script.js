document.addEventListener('DOMContentLoaded', function () {
    const dropdowns = document.querySelectorAll('.dropdown');

    dropdowns.forEach(function (dropdown) {
        dropdown.addEventListener('click', function () {
            // Alternar a classe 'active' no dropdown clicado
            dropdown.classList.toggle('active');
        });
    });
});
