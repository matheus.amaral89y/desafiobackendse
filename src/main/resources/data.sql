DELETE FROM products;

INSERT INTO products (id, product_name, price) VALUES (1, 'Hamburger', 40.00);
INSERT INTO products (id, product_name, price) VALUES (2, 'Batatinha frita', 12);
INSERT INTO products (id, product_name, price) VALUES (3, 'Coca-Cola', 8);
INSERT INTO products (id, product_name, price) VALUES (4, 'Pizza', 60);
INSERT INTO products (id, product_name, price) VALUES (5, 'Agua', 6.05);
INSERT INTO products (id, product_name, price) VALUES (6, 'sobremesa', 2);
INSERT INTO products (id, product_name, price) VALUES (7, 'Sanduíche', 8);

DELETE FROM users;
INSERT INTO users (id, name) VALUES(1, "Matheus");
INSERT INTO users (id, name) VALUES(2, "Guilherme");
INSERT INTO users (id, name) VALUES(3, "Arthur");
INSERT INTO users (id, name) VALUES(4, "Felipe");