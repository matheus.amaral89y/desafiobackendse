package com.desafio.backendse.services;

import com.desafio.backendse.application.model.OrderModel;
import com.desafio.backendse.application.model.ProductModel;
import com.desafio.backendse.application.reponses.CalcResponse;
import com.desafio.backendse.domain.enums.DiscountAndServiceChargeType;
import com.desafio.backendse.domain.enums.OrderType;
import com.desafio.backendse.domain.services.OrderServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class OrderServicesTest {
    @InjectMocks
    OrderServices orderServices;
    OrderModel model;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        model = new OrderModel();
        model.setId(1L);
        model.setType(OrderType.APP);
        model.setTotalPrice(new BigDecimal(50));
        model.setDiscount(new BigDecimal(20));
        model.setServiceCharge(new BigDecimal(8));

        List<ProductModel> productModelList = new ArrayList<>();
        ProductModel product1 = new ProductModel();
        product1.setId(1L);
        product1.setProductName("Hamburger");
        product1.setUserId(1L);
        product1.setPrice(new BigDecimal(40));
        product1.setUserName("Matheus A.");

        ProductModel product2 = new ProductModel();
        product2.setId(2L);
        product2.setProductName("Sobremesa");
        product2.setUserId(1L);
        product2.setPrice(new BigDecimal(2));
        product2.setUserName("Matheus A.");

        ProductModel product3 = new ProductModel();
        product3.setId(3L);
        product3.setProductName("Sanduíche");
        product3.setUserId(2L);
        product3.setPrice(new BigDecimal(8));
        product3.setUserName("Guilherme");

        productModelList.add(product1);
        productModelList.add(product2);
        productModelList.add(product3);

        model.setProducts(productModelList);
    }

    @Test
    public void testCalculatePaymentAmountWithCash() {
        model.setDiscountAndServiceChargeType(DiscountAndServiceChargeType.CASH);
        List<CalcResponse> actualResponse = orderServices.calculatePaymentAmount(model);

        BigDecimal amountToPayUser1 = findAmountToPayForUser(actualResponse, 1L);
        BigDecimal amountToPayUser2 = findAmountToPayForUser(actualResponse, 2L);

        assertEquals(new BigDecimal("31.92"), amountToPayUser1);
        assertEquals(new BigDecimal("6.08"), amountToPayUser2);
    }

    @Test
    public void testCalculatePaymentAmountWithPercentage() {
        model.setDiscountAndServiceChargeType(DiscountAndServiceChargeType.PERCENT);
        List<CalcResponse> actualResponse = orderServices.calculatePaymentAmount(model);

        BigDecimal amountToPayUser1 = findAmountToPayForUser(actualResponse, 1L);
        BigDecimal amountToPayUser2 = findAmountToPayForUser(actualResponse, 2L);

        assertEquals(new BigDecimal("36.29"), amountToPayUser1);
        assertEquals(new BigDecimal("6.91"), amountToPayUser2);
    }

    private BigDecimal findAmountToPayForUser(List<CalcResponse> calcResponses, Long userId) {
        return calcResponses.stream()
                .filter(response -> response.getUserId().equals(userId))
                .findFirst()
                .map(CalcResponse::getAmountToPay)
                .orElse(BigDecimal.ZERO);
    }
}
